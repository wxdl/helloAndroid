package com.wxdl.helloand;


import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.IOException;

/**
 * Created by wxdl on 17-9-12.
 */

public class Activity_Camera extends AppCompatActivity implements SurfaceHolder.Callback {
    public static final String TAG="Activity_Camera";
    private SurfaceView cameraView;
    private Camera camera;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate");
        setContentView(R.layout.activity_camera);
        cameraView = (SurfaceView) findViewById(R.id.cameraView);
        cameraView.getHolder().addCallback(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart");
    }

    @Override
    protected void onStop() {
        Log.d(TAG,"onStop");
        super.onStop();
    }

    public void toggleCamera(View view) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG,"surfaceCreated");
        camera = Camera.open();
        try {
            camera.setPreviewDisplay(cameraView.getHolder());
        } catch (IOException e) {
            Log.d(TAG,"previewDisplay");
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG,"surfaceChanged");
        camera.setParameters(camera.getParameters());
        camera.setPreviewCallbackWithBuffer(new Camera.PreviewCallback() {
            @Override
            public void onPreviewFrame(byte[] data, Camera camera) {
                Log.d(TAG,"onPreviewFrame");
            }
        });
        camera.startPreview();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG,"surfaceDestroyed ");

    }
}
