package com.wxdl.helloand.firstDay;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.wxdl.helloand.R;

public class ResultActivity extends AppCompatActivity {
    public static final String TAG="ResultActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
    }

    public void finish(View view) {
        Log.i(TAG,"finish Activity");
        Intent intent = new Intent();
        intent.putExtra("resultData","resultActivity is Perfect!!!");
        setResult(RESULT_OK,intent);
        finish();
    }
}
