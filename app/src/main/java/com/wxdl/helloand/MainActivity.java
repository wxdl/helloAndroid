package com.wxdl.helloand;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.wxdl.helloand.firstDay.IndexActivity;

public class MainActivity extends AppCompatActivity {
    private static final String TAG="MainActivity";
    private Button button ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.bt_selector);
        Drawable drawable = getResources().getDrawable(R.drawable.bt_selector);
        button.setBackground(drawable);
    }

    public void sendNotify(View view) {
        Intent intent = new Intent(this,HelloNotify.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager notificationManager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        Notification notification = new Notification.Builder(this)
                .setContentText("弹出了一个通知")
                .setContentTitle("通知,通知")
                .setSmallIcon(R.drawable.ic_stat_name)
                .setWhen(System.currentTimeMillis())
                .setTicker("tricker")
                .setContentIntent(pendingIntent)
                .build();
        notificationManager.notify(1,notification);
    }

    public void openCamera(View view) {
        Log.d(TAG,"openCamera");
        Intent intent = new Intent(this,Activity_Camera.class);
        startActivity(intent);
    }

    public void finish(View view) {
        Log.d(TAG,"finish activity");
        finish();
    }

    public void secondActivity(View view) {
        Log.i(TAG,"open secondActivity");
        Intent second = new Intent(this,SecondActivity.class);
        startActivity(second);
    }

    public void startImplicitAct(View view) {
        Intent implicit  = new Intent("com.wxdl.openImplicit");
        implicit.addCategory("com.wxdl.myCategory");
        startActivity(implicit);
    }

    public void firstDay(View view) {
        Intent intent = new Intent(this, IndexActivity.class);
        startActivity(intent);
    }
}
