package com.wxdl.helloand.intent;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.wxdl.helloand.R;

public class OpenAction extends AppCompatActivity {

    private TextView tv_openAction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.open_action_activity);
        tv_openAction = (TextView) findViewById(R.id.tv_openAction);
        Uri data = getIntent().getData();
        tv_openAction.setText(data.toString());
    }


}
