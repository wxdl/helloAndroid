package com.wxdl.helloand.intent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.wxdl.helloand.R;

/**
 * Created by wxdl on 17-9-16.
 */

public class ImplicitActivity extends AppCompatActivity {
    public static final String TAG=ImplicitActivity.class.getSimpleName();
    public static final String ACTION="OPEN_IMPLICIT";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"oncreate");
        setContentView(R.layout.implict_activity);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void OpenAction(View view) {
        Log.d(TAG,"openAction");
        Intent action=new Intent(Intent.ACTION_VIEW);
        action.setData(Uri.parse("http://www.baidu.com"));
        startActivity(action);
    }
}
